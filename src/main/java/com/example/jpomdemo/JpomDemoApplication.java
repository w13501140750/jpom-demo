package com.example.jpomdemo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class JpomDemoApplication {

    public static void main(String[] args) {
        SpringApplication.run(JpomDemoApplication.class, args);
    }

}
